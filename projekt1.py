import math

print ('Dla równania kwadratowego ax2+bx+c=0')
a=float(input('podaj wartość parametru a: '))
b=float(input('podaj wartość parametru b: '))
c=float(input('podaj wartość parametru c: '))

if a != 0:
    delta = (b*b)-(4*a*c)
    if delta > 0:
        delta2 = math.sqrt(delta)
        x1 = (-b-delta2)/(2*a)
        x2 = (-b+delta2)/(2*a)
        print (f"x1 = {x1} x2= {x2}")
    elif delta == 0:
        x0 = -b/(2*a)
        print(f'x0 = {x0}')
    else:
        print('brak rozwiązań')
else:
    print("Twoja funkcja to funkcja liniowa")
